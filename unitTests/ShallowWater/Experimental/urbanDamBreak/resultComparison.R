# Comparing results for the urban dam break case by Soares-Frazao.
library(SERGHEI)
CPU_CORES = 8

rootpath=SERGHEI_UNITTESTS_DIR

dt = 0.1

# set directories
basedir = file.path(SERGHEI_UNITTESTS_DIR,"ShallowWater/Experimental/urbanDamBreak")
outname = "output"
inpath = file.path(basedir,"input")
simoutdir = file.path(basedir,outname)

# read gauge coordinates
gaugePoints = SpatialPoints(read.table(file.path(basedir,"Reference/gaugeCoordinates.txt"),header=T))

# read observational data
obs = read.table(file.path(basedir,"Reference/observedDepth.txt"),header=T)

# read simulation gauges
sim = SERGHEI::readGauges(inpath,simoutdir)
sim = SERGHEI::gaugeVelocities(sim)

#ig=10
#simg = sim[[ig]] 
#yrange = c(min(obs[,ig+1],simg[,2]),max(obs[,ig+1],simg[,2]))
#plot(simg$time,simg[,2]*1000,t="l",xlab="Time [s]",ylab="Depth [mm]",ylim=yrange*1000)
#lines(obs$time,obs[,ig+1]*1000,col="red")
#
#plot(simg$time,vel,t="l",xlab="Time [s]",ylab="Velocity [m/s]")

# read simulation lines
lines = SERGHEI::readObservationLines(inpath,simoutdir)
times = SERGHEI::getTimesForLine(lines$line0)
lines = SERGHEI::getLinesCoordinates(lines,simoutdir)

# define a time to plot a profile
plotTime = 5
tt=which(times==plotTime)
myline=lines$line0[[tt]]
myGline=lines$line0$geom

# get the simulated gauge profile
profileGP = which(gaugePoints@coords[,2] == 0.2)
simProfile=data.frame("x"=gaugePoints[profileGP]@coords[,1],"y"=gaugePoints[profileGP]@coords[,2])
tt=which(myGauge$time == plotTime)
simProfile$h=sapply(sim[profileGP],function(g,tt) g$h[tt],tt=tt)

# get the experimental profile
obsProfile = simProfile
obsProfile$h = NULL
tto=which(obs$time == plotTime)
obsProfile$h = t(obs[tto,(profileGP+1)])

# plot a profile
Bstart = seq(5,by=0.4,length.out=5)
Bend = seq(5.3,by=0.4,length.out=5)

setEPS(width=7,height=6)
postscript(file.path(simoutdir,"h_profile_y0_2.eps"))
plot(myGline$x,myline$h,t="l",xlab="x [m]",ylab="h [m]",col="blue",lwd=2,
     panel.first = rect(Bstart, -1e6, Bend, 1e6, col='grey', border=NA),
)
points(obsProfile$x,obsProfile$h,t="p",col="black",pch=16,cex=1.5)
points(simProfile$x,simProfile$h,col="blue",pch=1,cex=1.5)
legend(6.6,.16, legend=c("Experimental", "Simulated"),
       col=c("black", "blue"), lty=c(1,1), cex=1.2)
dev.off()

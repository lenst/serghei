# SERGHEI

Simulation Environment for Geomorphology, Hydrodynamics and
Ecohydrology in Integrated form

# Dependencies

SERGHEI has the following dependencies:

+ [Kokkos](https://github.com/kokkos/kokkos) handles the
parallelization
+ [Parallel NetCDF](https://github.com/Parallel-NetCDF/PnetCDF) writes
output files
+ We use [R](https://www.r-project.org/) scripts for postprocessing (optional)

# Installation

## Environment configuration
You need to set the SERGHEIPATH environment variable to your local SERGHEI root path. 
```
export SERGHEIPATH=/path/to/serghei
```
Keep in mind doing so is not a persisting configuration.
For persisting configuration, you can set this in your `.bashrc` file. 

Paths in the build scripts, but also in other workflows in SERGHEI use the SERGHEIPATH environment variable.

SERGHEI can be installed through `make`. Before attempting to compile, the correct paths pointing to `Kokkos` and `PNetCDF` must be included in the `src/Makefile`. The default paths for these are `$SERGHEIPATH/Kokkos` and `$SERGHEIPATH/PnetCDF`. If you have these dependencies elsewehre, point the variable `PNETCDF_PATH` to the `PNetCDF` base folder and the variable `KOKKOS_PATH` to the `Kokkos` base folder.

To install SERGHEI, use either one of these:

+ `make arch=cpu`: compiles the code for CPU (OpenMP + MPI)
+ `make arch=gpu device=DEVICESM`: compiles the code for GPU (CudaUVM + MPI). DEVICESM is the keyword for the device architecture for your GPUs. The list of architecture keywords can be found 
[here](https://github.com/kokkos/kokkos/wiki/Compiling#table-43-architecture-variables). Commonly used may be, for example Ampere80, Volta70, Pascal61, etc.

This places an executable `serghei` into the directory `bin` in the
base folder of SERGHEI.

Further commands:
+ `make clean`: cleans the program objects and the exe file
+ `make kclean`: cleans the prgram objects, the exe file and the Kokkos
objects

## Model component compilation flags
SERHGEI's Makefile includes flags to control which model components are compiled. The default configuration of SERGHEI components is hardcoded. This flags will allow you to control which model components are actually compiled. The possible flags are commented inside the Makefile, therefore, unless you either uncomment some of them, or pass them to Make through the command line, you will get the default compilation configuration.
You can control through the command line the setup, for example 
```
$ make arch=cpu SERGHEI_SUBSURFACE_MODEL=1
``` 
will compile SERGHEI for CPUs, including the subsurface solver. 

Another example, to compile for Volta GPUs, including subsurface and particle tracking modules is
```
$ make arch=gpu device=Volta70 SERGHEI_SUBSURFACE_MODEL=1 SERGHEI_PARTICLE_TRACKING=1
``` 

# Testing installation (Unit Tests)
SERGHEI includes the `unitTests` folder, which includes the `runUnitTest` script to run basic unit tests inlcuded in the `unitTests` folder. None of these test cases should fail.


### Usage
This script allow several options. The following command will simply compile and run
```
$ bash runUnitTest
```
The full options set is
```
$ bash runUnitTest -bd -g GPUDEVICE -hk -s scriptName -v
```
-b enables benchmarking against reference solutions. It requires for `Rscript` to be available, and for the SERGHEI R package to be built and installed.

-d launches a dry run for the script, i.e., no simulations will actually be launched

-g compiles for GPU. Requires GPUDEVICE specified according to naming scheme (see Makefile)

-h enables tests designed for HPC environments. You should not attempt to run this on small systems.

-k overrides the default of cleaning the current compilation (i.e., will not run `make clean`)

-s uses a resource manager script configuration (for HPC systems). Requires script filename

-v verbose mode (for debugging)

# Running SERGHEI

Once installed, SERGHEI can be invoked by:

```
$ mpirun -n N ./serghei inputDir/ outputDir/ M
```

where

+ `N`: number of MPI tasks (subdomains). This must be in accordance
with the partition chosen in `parameters.input`
+ `inputDir`: directory where the input files are located
+ `outputDir`: directory where the output files will be located
+ `M`: number of threads (OpenMP) or number of GPUs per resource set
(GPUs)

## Examples

To run the test case located at `cases/paraboloid2`, execute within
the `bin` directory:

```
$ mpirun -n 2 ./serghei ../cases/paraboloid2/ output/ 4
```

Depending on the architecture, this command causes different things to
happen:

1. If the code has been compiled for CPU, this means that it would be
2 subdomains (MPI tasks) parallelized with 4 threads per subdomain
(OpenMP).
2. If the code has been compiled for GPU, this means that it would be
8 subdomains (MPI tasks). The code is run on 2 nodes, each of them
containing 4 GPUs.

Similarly, the use of `mpirun` is conditioned to the execution with
MPI and the corresponding architecture. For example, the code can be
run just using:

```
$ ./serghei ../cases/paraboloid2/ output/ 1
```

# Known issues

+ The `clang` compiler may fail to correctly load the OpenMP
library. Thus, if SERGHEI is compiled with `clang`, OpenMP may not be
available.
+ `gcc-10` has trouble compiling Parallel NetCDF and throws a type
  mismatch errors. The errors can be turned into warnings by passing
  ```
  FCFLAGS="-fallow-argument-mismatch" FFLAGS="-fallow-argument-mismatch"
  ```
  to `configure` and `make`. See [this github issue](https://github.com/Unidata/netcdf-fortran/issues/212).

# How to cite 
You can refer to the [preprint](https://gmd.copernicus.org/preprints/gmd-2022-208/) for the shallow water module SERGHEI-SWE.
```
@Article{caviedes2022,
AUTHOR = {Caviedes-Voulli\`eme, D. and Morales-Hern\'andez, M. and Norman, M. R. and \"Ozgen-Xian, I.},
TITLE = {SERGHEI (-SWE) v1.0: a performance portable HPC shallow water solver for hydrology and environmental hydraulics},
JOURNAL = {Geoscientific Model Development Discussions},
VOLUME = {2022},
YEAR = {2022},
PAGES = {1--44},
URL = {https://gmd.copernicus.org/preprints/gmd-2022-208/},
DOI = {10.5194/gmd-2022-208}
}
```

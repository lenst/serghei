#####################################################
## Makefile to build SERGHEI
## Copyright SERGHEI Workgroup 2019-2022
#####################################################
## IMPORTANT NOTICE
## Make sure you have set the SERGHEIPATH environmental variable to the serghei directory

## USAGE
## Invoke make with your prefered architecture, for example
## make arch=cpu
## make arch=gpu device=Volta70
## If you invoke simply "make", cpu is the default architecture
## For GPU compilation device is mandatory

.PHONY: main

## DEFINE MODEL SETUP
## In this section you can control the compilation of model components
## The defaults settings are hardcoded in SERGHEI
## By uncommenting and defining these flags you can manually control which components are compiled
## You can also pass these definitions from command line, e.g., make SERGHEI_PARTICLE_TRACKING=1
#SERGHEI_SUBSURFACE_MODEL=0
#SERGHEI_PARTICLE_TRACKING=0
#SERGHEI_VEGETATION_MODEL=0
#SERGHEI_TOOLS=0
#SERGHEI_FRICTION_MODEL=1

## Debugging flags
#SERGHEI_DEBUG_PARALLEL_DECOMPOSITION=0
#SERGHEI_DEBUG_WORKFLOW=0
#SERGHEI_DEBUG_KOKKOS_SETUP=0
#SERGHEI_DEBUG_BOUNDARY=0
#SERGHEI_DEBUG_DT=0
#SERGHEI_DEBUG_TOOLS=0
#SERGHEI_DEBUG_MASS_CONS=0
#SERGHEI_DEBUG_INFILTRATION=0

## Define library paths
PNETCDF_PATH=${SERGHEIPATH}/PnetCDF/src
KOKKOS_PATH = ${SERGHEIPATH}/kokkos
KOKKOS_SRC_PATH = ${KOKKOS_PATH}

## by default build for cpu, alternative should be gpu
ifndef ($arch)
	arch = cpu
	archCheck = checkCPU
endif

ifeq ($(SERGHEI_DEBUG_PARALLEL_DECOMPOSITION),1)
debug=yes
endif
ifeq ($(SERGHEI_DEBUG_WORKFLOW),1)
debug=yes
endif
ifeq ($(SERGHEI_DEBUG_KOKKOS_SETUP),1)
debug=yes
endif
ifeq ($(SERGHEI_DEBUG_BOUNDARY),1)
debug=yes
endif
ifeq ($(SERGHEI_DEBUG_DT),1)
debug=yes
endif
ifeq ($(SERGHEI_DEBUG_TOOLS),1)
debug=yes
endif
ifeq ($(SERGHEI_DEBUG_MASS_CONS),1)
debug=yes
endif
ifeq ($(SERGHEI_DEBUG_INFILTRATION),1)
debug=yes
endif

KOKKOS_DEBUG=no
ifeq ($(KOKKOS_DEBUG),yes)
debug=yes
endif

########### LEAVE THIS ALONE ##############
# Define binary folders and build executable name
BIN_DIR = ../bin
EXE = $(BIN_DIR)/serghei
dummy_build_folder := $(shell mkdir -p $(BIN_DIR))


CXXFLAGS = -O3 -I${PNETCDF_PATH}/include
MPICXX=`which mpic++`

ifdef SERGHEI_SUBSURFACE_MODEL
	SERGHEI_MODEL_FLAGS += -DSERGHEI_SUBSURFACE_MODEL=$(SERGHEI_SUBSURFACE_MODEL)
endif

ifdef SERGHEI_PARTICLE_TRACKING
	SERGHEI_MODEL_FLAGS += -DSERGHEI_PARTICLE_TRACKING=$(SERGHEI_PARTICLE_TRACKING)
endif
ifdef SERGHEI_VEGETATION_MODEL
	SERGHEI_MODEL_FLAGS += -DSERGHEI_VEGETATION_MODEL=$(SERGHEI_VEGETATION_MODEL)
endif
ifdef SERGHEI_TOOLS
	SERGHEI_MODEL_FLAGS += -DSERGHEI_TOOLS=$(SERGHEI_TOOLS)
endif
ifdef SERGHEI_DEBUG_PARALLEL_DECOMPOSITION
	SERGHEI_MODEL_FLAGS += -DSERGHEI_DEBUG_PARALLEL_DECOMPOSITION=$(SERGHEI_DEBUG_PARALLEL_DECOMPOSITION)
endif
ifdef SERGHEI_DEBUG_WORKFLOW
	SERGHEI_MODEL_FLAGS += -DSERGHEI_DEBUG_WORKFLOW=$(SERGHEI_DEBUG_WORKFLOW)
endif
ifdef SERGHEI_DEBUG_KOKKOS_SETUP
	SERGHEI_MODEL_FLAGS += -DSERGHEI_DEBUG_KOKKOS_SETUP=$(SERGHEI_DEBUG_KOKKOS_SETUP)
endif
ifdef SERGHEI_DEBUG_BOUNDARY
	SERGHEI_MODEL_FLAGS += -DSERGHEI_DEBUG_BOUNDARY=$(SERGHEI_DEBUG_BOUNDARY)
endif
ifdef SERGHEI_FRICTION_MODEL
	SERGHEI_MODEL_FLAGS += -DSERGHEI_FRICTION_MODEL=$(SERGHEI_FRICTION_MODEL)
endif
ifdef SERGHEI_DEBUG_DT
	SERGHEI_MODEL_FLAGS += -DSERGHEI_DEBUG_DT=$(SERGHEI_DEBUG_DT)
endif
ifdef SERGHEI_DEBUG_TOOLS
	SERGHEI_MODEL_FLAGS += -DSERGHEI_DEBUG_TOOLS=$(SERGHEI_DEBUG_TOOLS)
endif
ifdef SERGHEI_DEBUG_MASS_CONS
    SERGHEI_MODEL_FLAGS += -DSERGHEI_DEBUG_MASS_CONS=$(SERGHEI_DEBUG_MASS_CONS)
endif
ifdef SERGHEI_DEBUG_INFILTRATION
    SERGHEI_MODEL_FLAGS += -DSERGHEI_DEBUG_INFILTRATION=$(SERGHEI_DEBUG_INFILTRATION)
endif
ifeq ($(debug),yes)
	SERGHEI_MODEL_FLAGS += -g
endif

ifeq ($(arch),cpu)
	CXX = ${MPICXX}
	CXXFLAGS += -DKOKKOS_ENABLE_CXX17 -DKOKKOS_CXX_STANDARD=c++17 -std=c++17
	KOKKOS_DEVICES = "OpenMP"
	KOKKOS_OPTIONS = "aggressive_vectorization,disable_profiling"
endif

ifeq ($(arch),gpu)
	archCheck=checkGPU
	CXX = ${KOKKOS_PATH}/bin/nvcc_wrapper -x cu -ccbin ${MPICXX}
	#CXX = clang
	CXXFLAGS += -DKOKKOS_ENABLE_CXX17 -DKOKKOS_CXX_STANDARD=c++17 -std=c++17
	KOKKOS_DEVICES = "Cuda"
	KOKKOS_OPTIONS += "disable_profiling"
	KOKKOS_CUDA_OPTIONS += "enable_lambda"
endif

CXXFLAGS += $(SERGHEI_MODEL_FLAGS)
LINK = ${CXX}

LDFLAGS = -L${PNETCDF_PATH}/libs/.libs -lpnetcdf

SRC = driver.cpp
OBJ = $(BIN_DIR)/$(notdir $(SRC:.cpp=.o))

# what does this do?
DEPFLAGS = -M

# exports all variables to be used by sub-make
export

main: $(archCheck) $(arch)

checkCPU:

checkGPU:
ifndef device
	$(error GPU architecture undefined. Please set e.g., device=Volta70. You can choose from Ampere80, Volta70, Pascal61, Maxwell50, etc... See for more options  https://github.com/kokkos/kokkos/wiki/Compiling#table-43-architecture-variables)
endif
KOKKOS_ARCH = ${device}
# For device keywords see https://github.com/kokkos/kokkos/wiki/Compiling#table-43-architecture-variables



cpu:
	@echo -e "\033[92mBuilding for CPU...\033[0m"
	@make -j4 -f Makefile.cpu

gpu:
	@echo -e "\033[92mBuilding for GPU...\033[0m"
	@make -j4 -f Makefile.gpu



clean:
	rm -f *.gch *.o *.dat serghei
	rm -rf $(BIN_DIR)

kclean: clean
	rm -f KokkosCore_config.h KokkosCore_config.tmp libkokkos.a

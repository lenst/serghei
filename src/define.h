
#ifndef _CONST_H_
#define _CONST_H_

#include "math.h"
#include "const.h"
#include <Kokkos_Core.hpp>

typedef double         real;
typedef unsigned long ulong;
typedef unsigned int  uint;

#ifdef __NVCC__
  typedef Kokkos::View<real*     ,Kokkos::LayoutRight,Kokkos::Device<Kokkos::Cuda,Kokkos::CudaUVMSpace>> realArr;
  typedef Kokkos::View<int*     ,Kokkos::LayoutRight,Kokkos::Device<Kokkos::Cuda,Kokkos::CudaUVMSpace>> intArr;
  typedef Kokkos::View<bool*     ,Kokkos::LayoutRight,Kokkos::Device<Kokkos::Cuda,Kokkos::CudaUVMSpace>> boolArr;
#else
  typedef Kokkos::View<real*     ,Kokkos::LayoutRight> realArr;
  typedef Kokkos::View<int*     ,Kokkos::LayoutRight> intArr;
  typedef Kokkos::View<bool*     ,Kokkos::LayoutRight> boolArr;
#endif

#ifdef __NVCC__
#define _HOSTDEV __host__ __device__
#else
#define _HOSTDEV
#endif

KOKKOS_INLINE_FUNCTION real operator"" _fp( long double x ) {
  return static_cast<real>(x);
}

KOKKOS_INLINE_FUNCTION double mypow ( double const x , double const p ) { return pow (x,p); }
KOKKOS_INLINE_FUNCTION float  mypow ( float  const x , float  const p ) { return powf(x,p); }
KOKKOS_INLINE_FUNCTION double mysqrt( double const x ) { return sqrt (x); }
KOKKOS_INLINE_FUNCTION float  mysqrt( float  const x ) { return sqrtf(x); }
KOKKOS_INLINE_FUNCTION double myfabs( double const x ) { return fabs (x); }
KOKKOS_INLINE_FUNCTION float  myfabs( float  const x ) { return fabsf(x); }

template <class T> KOKKOS_INLINE_FUNCTION T min( T const v1 , T const v2 ) {
  if (v1 < v2) { return v1; }
  else         { return v2; }
}
template <class T> KOKKOS_INLINE_FUNCTION T max( T const v1 , T const v2 ) {
  if (v1 > v2) { return v1; }
  else         { return v2; }
}

template <class T> KOKKOS_INLINE_FUNCTION int sgn(T const val) {
    return (T(0) < val) - (val < T(0));
}

class SergheiTimers{
public:
  Kokkos::Timer serghei;
  Kokkos::Timer out;
  Kokkos::Timer swe;
  real total=0;
  real Tinit=0;
  real Tout=0;
  real Tswe=0;
  real Traininf=0;
  real Tsweflux=0;
  real Texchange=0;
  real Tintegrate=0;
};

#endif

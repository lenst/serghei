/* -*- mode: c++; c-default-style: "linux" -*- */

#include "stdlib.h"
#include <iostream>
#include <string>
#include "define.h"
#include "Domain.h"
#include "Parallel.h"
#include "Parser.h"
#include "State.h"
#include "BC.h"
#include "Initializer.h"
#include "TimeIntegrator.h"
#include "FileIO.h"
#include "Exchange.h"
#include "SWSourceSink.h"
#include "Subsurface.h"
#include "DomainIntegrator.h"
#include "Vegetation.h"
#include "ParticleTracking.h"
#include "tools.h"


int main(int argc, char** argv) {

	#if SERGHEI_DEBUG_WORKFLOW
	std::cerr << GGD "Start" << std::endl;
	#endif


	SergheiTimers timers;

	Kokkos::InitArguments args;

	//these variables are needed to call  initializeMPI
	Parallel       par;
	Initializer    init;

	//init the timer
	timers.serghei.reset();

	//Read input and create output
	if (argc != 4){
		std::cerr << RERROR "The program is run as: ./nprogram inputFolder/ outputFolder/ Nthreads" << "\n";
		return 0;
	}
	std::string inFolder = argv[1];
	std::string outFolder = argv[2];
	par.nthreads=atoi(argv[3]);

	#if SERGHEI_DEBUG_WORKFLOW
	std::cerr << GGD "Initialising MPI AND CUDA" << std::endl;
	#endif


	// Initialize MPI According to the documentation, MPI_Init should be called befored Kokkos::initialize
	init.initializeMPI( &argc , &argv , par );

	#ifdef __NVCC__
		args.device_id=par.myrank%par.nthreads;
	#else
		if(par.nthreads!=0){
			args.num_threads = par.nthreads;
		}
	#endif
	#if SERGHEI_DEBUG_KOKKOS_SETUP
		printKokkosInitArguments(args,par);
		#if __NVCC__
			printKokkosCuda(args,par);
		#endif
	#endif

	#if SERGHEI_DEBUG_WORKFLOW
	std::cerr << GGD "Initialising Kokoks - rank " << par.myrank << std::endl;
	#endif
	// INITIALIZE KOKKOS
	Kokkos::initialize(args);


	#if SERGHEI_DEBUG_WORKFLOW
		std::cerr << GGD "Program instantiated, creating objects - rank " << par.myrank << std::endl;
	#endif

	{ //these scope guards are needed to avoid annoying warnings

		// Create the model objects, these are created one for each MPI rank (or subdomain)
		State          state;
		SourceSinkData ss;
		ExternalBoundaries ebc;
		Domain         dom;
		DomainSubsurface	domsub;
		Parser         parser;
		FileIO         io;
		Exchange       exch;
		TimeIntegrator tint;
		surfaceIntegrator sint;
		boundaryIntegrator bint;
		#if SERGHEI_PARTICLE_TRACKING
		ParticleTracker parTrack;
		#endif
		#if SERGHEI_TOOLS
		Observations obs;
		#endif
		#if SERGHEI_SUBSURFACE_MODEL
		SubsurfaceState 	statesub;
		#endif
		real oldVolume,newVolume, diffVolume;
		real accumDt=0.0;


		// Initialize the model
		if(!init.initialize(state, ss, ebc, dom, par, tint, sint, bint, parser, exch, io, inFolder, outFolder)){
			std::cerr << RERROR "Unable to start the simulation" << "\n";
			return 0;
		};

		// Initialize subsurface model if activated
		#if SERGHEI_SUBSURFACE_MODEL
		if(!init.initializeSubsurface(statesub, state, domsub, dom, par, tint, parser, io, inFolder, outFolder)){
		  std::cerr << RERROR "Unable to initialise the subsurface domain" << "\n";
		  return 0;
		};
		#endif

		#if SERGHEI_TOOLS
  	if(!obs.readInputFiles(inFolder,par)) return 0;
		if(!obs.configure(dom,outFolder)) return 0;	// observations for surface domain
		//obs.printGauges(dom);
		obs.update(state,par,dom.dt);

		if( par.masterproc){
			obs.writeLinesSamplingCoordinates(outFolder);
			obs.writeGauges(dom.etime);
			obs.writeLines(dom.etime);
		}
		#endif

		#if SERGHEI_DEBUG_WORKFLOW

		for (int k = 0; k < ebc.extbc.size(); k ++) {
		  std::cerr << GGD << __FILE__ << ":" << __LINE__ << "\tExtBC[" << k << "]: " << ebc.extbc[k].bcvals(0) << ", " << ebc.extbc[k].bcvals(1) << ", " << ebc.extbc[k].bcvals(2) << std::endl;
		}

		std::cerr << GGD "Initialisation finished, starting to run main loop" << std::endl;
		for(int i = 0; i < ebc.extbc.size(); i ++) {
		  std::cerr << GGD "ncellsBC for segment " << i << ": " << ebc.extbc[i].ncellsBC << "\n";
		}
		#if SERGHEI_DEBUG_BOUNDARY
		bint.integrate(ebc.extbc,1);
		std::cerr << GGD "ncellsBC (integrated) " << bint.ncellsBC << "\n";
		std::cerr << GGD "outflow discharge (integrated) " << bint.outflowDischarge << "\n";
		std::cerr << GGD "outflow accumulated (integrated) " << bint.outflowAccumulated << std::endl;
		#endif
		#endif

		//integrator at the beginning or the simulation
		sint.integrate(state,dom,ss);

		// Write initial time series data
		io.writeTimeSeriesIni(state,dom,par,ss,sint,bint,ebc.extbc,outFolder);

		// capture initialisation time
		if (par.masterproc) timers.Tinit = timers.serghei.seconds();

		if (par.masterproc) std::cerr << "\n" << GOK "SIMULATION STARTS\n";


		while (dom.etime < dom.simLength) {

			//previous mass
			oldVolume=sint.surfaceVolumeG;

			bint.integrate(ebc.extbc,1);//has to be called here (previous time step) with mode==1 (boundary flows)

			tint.stepForward(state, ss, ebc.extbc, dom, exch, par, io, timers);
			timers.swe.reset();

			oldVolume+=(bint.inflowDischargeG - bint.outflowDischargeG)*dom.dt; //Boundary fluxes with the new dt

			bint.integrate(ebc.extbc,0);//called here with mode==0 (adjusted volume)

			oldVolume+=bint.adjustedVolumeG; //Some mass changes can occur through the boundaries


			sint.integrate(state,dom,ss); //new mass after the new time step integration
			oldVolume+= (sint.rainFluxG-sint.infFluxG)*dom.dt; //after integrate, we have to sum the rain and inf mass

			timers.Tintegrate += timers.swe.seconds();

			//new mass
			newVolume=sint.surfaceVolumeG;

			if(fabs(oldVolume)>TOL12){
				diffVolume=(newVolume-oldVolume)/oldVolume*100.;
			}else{
				diffVolume=0.0;
			}
			dom.etime += dom.dt;
			dom.nIter++;
			dom.countIterDt++;
			accumDt+=dom.dt;

			if (dom.nIter%io.nScreen==0 || fabs(dom.etime - io.numOut*io.outFreq) < TOL12) {
				if (par.masterproc) {
					timers.out.reset();
					std::cerr << std::fixed;
					std::cerr << GSTAR "TIME: " << dom.etime << " average dt: " << accumDt/dom.countIterDt <<"\n";
					std::cerr.precision(9);
					std::cerr << std::scientific;
					std::cerr << "     Diff Volume: " << diffVolume <<"\n";
					std::cerr << std::fixed;
					std::cerr.precision(12);
					std::cerr << "     Inflow Discharge: " << bint.inflowDischargeG <<"\n";
					std::cerr << "     Outflow Discharge: " << bint.outflowDischargeG <<"\n";

					if(fabs(diffVolume)>TOL8){
						std::cerr << YEXC "   Old Volume:\t" << oldVolume <<"\n";
						std::cerr << YEXC "   New Volume:\t" << newVolume <<"\n";
						std::cerr << YEXC "   Diff Volume:\t" << newVolume-oldVolume <<"\n";
						std::cerr << YEXC "   Inflow Volume:\t" << bint.inflowDischargeG*dom.dt <<"\n";
						std::cerr << YEXC "   Outflow Volume:\t" << bint.outflowDischargeG*dom.dt <<"\n";
						std::cerr << YEXC "   Adjusted Volume:\t" << bint.adjustedVolumeG <<"\n";
						std::cerr << YEXC "   Rain Volume:\t" << sint.rainFluxG*dom.dt <<"\n";
						std::cerr << YEXC "   Inf Volume:\t" << sint.infFluxG*dom.dt <<"\n";
						#if SERGHEI_DEBUG_MASS_CONS > 1
                            getchar();
                        #endif
					}

				}
				if(fabs(dom.etime - io.numOut*io.outFreq) < TOL12){
					io.output(state, dom, ss, par,outFolder);
					if(par.masterproc) std::cerr << GIO "File " << io.numOut-1 << " written" <<"\n"; //io.numOut already updated
					if(par.masterproc) timers.Tout+=timers.out.seconds();
				}
				if(par.masterproc) std::cerr << "-------------------------------------------------\n";
				dom.countIterDt=0;
				accumDt=0.0;


			}



			#if SERGHEI_PARTICLE_TRACKING
			parTrack.update(dom,state);
			#endif

			if (dom.etime >= io.numObs*io.obsFreq) {
				timers.out.reset();
				#if SERGHEI_TOOLS
				obs.update(state,par,dom.dt);
				#endif

				io.writeTimeSeries(state,dom,par,sint,bint);
			  if (par.masterproc){
					#if SERGHEI_TOOLS
					obs.writeGauges(dom.etime);
					obs.writeLines(dom.etime);
					#endif
			  }
				timers.Tout += timers.out.seconds();
			}

		}

		if (par.masterproc){
			std::cerr << GOK "SIMULATION FINISHED\n";
			timers.total = timers.serghei.seconds();
			timers.Texchange = exch.exchangeTime;
			timers.Traininf = ss.timerRainInf;
			std::cerr << GOK "Time elapsed: " << timers.total << "\n";
			io.writeLogFile(dom,par,timers,outFolder);
		}

	io.closeOutputStreams();
	#if SERGHEI_TOOLS
	if(par.masterproc) obs.closeOutputStreams();
	#endif
	}


	Kokkos::finalize();

	MPI_Finalize();

}
